# Screego server Dockerfile
#
# https://screego.net/
# Build instructions: https://screego.net/#/development
# Original author: jmattheis
# Adapted by PCLL Team for Eole³ project

##
# Build app
##

FROM golang:alpine3.20 AS builder

ARG SCREEGO_VERSION=1.10.3
ARG GITREPO_URL="https://gitlab.mim-libre.fr/infrabricks/containers/screego/screego-sources.git"

RUN apk add --no-cache --update git yarn

WORKDIR /src

RUN git clone --depth 1 --branch v${SCREEGO_VERSION} \
    -c advice.detachedHead=false ${GITREPO_URL} && \
    cd $(basename -s .git "${GITREPO_URL}") && go mod download && \
    (cd ui && yarn install) && \
    (cd ui && yarn build) && \
    go build -ldflags "-X main.version=v${SCREEGO_VERSION} -X main.mode=prod" -o /screego ./main.go

##
# Final image
##

FROM scratch

LABEL org.opencontainers.image.title="screego" \
      org.opencontainers.image.description="Screen sharing" \
      org.opencontainers.image.vendor="screego.net" \
      org.opencontainers.image.licenses="GNU GPL-3.0" \
      org.opencontainers.image.source="https://github.com/screego/server" \
      org.opencontainers.image.maintainer="PCLL Team"

COPY --from=builder /screego /screego

USER 1001

EXPOSE 3478/tcp
EXPOSE 3478/udp
EXPOSE 5050
WORKDIR "/"
ENTRYPOINT [ "/screego" ]
CMD ["serve"]
