# Screego server container builder

This dockerfile builds a container for Screego server.

## Presentation

"In the past I’ve had some problems sharing my screen with coworkers using corporate chatting solutions like Microsoft Teams. I wanted to show them some of my code, but either the stream lagged several seconds behind or the quality was so poor that my colleagues couldn’t read the code. Or both.

That’s why I created screego. It allows you to share your screen with good quality and low latency. Screego is an addition to existing software and only helps to share your screen. Nothing else (:."

## Usage

Build command example

```
docker build -t local/screego:1.10.3 --build-arg="SCREEGO_VERSION=1.10.3" --progress=plain .
```

## Links

- [Official web site](https://screego.net/)
- [Github repo](https://github.com/screego/server)
